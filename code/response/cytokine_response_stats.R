#!/usr/bin/env Rscript

hlp = " See basic statistics on cytokine responses. "

# Libs
require(pheatmap)
require(reshape2)
require(ggplot2)
require(ggpubr)

# Paths
in.data = "./data/data.Robj"
out.dir = file.path("./output/response/")
dir.create(out.dir, recursive = TRUE, showWarnings = FALSE)
img.dir = file.path(out.dir, "img")
dir.create(img.dir, recursive = TRUE, showWarnings = FALSE)

# Parameters
alpha = 0.05

# Load data object
load(in.data)
obj = .GlobalEnv$obj
meta = obj$metadata

# Stimulations to be kept
modes = list(Trained=list(cytokines = c("IL1b", "IL6", "TNF"), stimuli = c("S. aureus")),
             Specific = list(cytokines = c("IFNg"), stimuli = c("M. tuberculosis")))


# Prepare data frame
C = obj$assays$Cytokines$data
cm = obj$assays$Cytokines$metadata
k1 = as.character(cm$Stimulus) %in% modes[["Trained"]][["stimuli"]] & 
  as.character(cm$Cytokine) %in% modes[["Trained"]][["cytokines"]]
k2 = as.character(cm$Stimulus) %in% modes[["Specific"]][["stimuli"]] & 
  as.character(cm$Cytokine) %in% modes[["Specific"]][["cytokines"]]
keep = k1 | k2
cm = cm[keep,]
C = C[,colnames(C) %in% row.names(cm)]
message(sprintf("Keeping %d responses", ncol(C)))

# Draw summary of expressions
df = melt(as.matrix(C))
colnames(df) = c("Sample", "Variable", "Value")
df$Sample = as.character(df$Sample)
df$Variable = as.character(df$Variable)
df = cbind(df, cm[df$Variable,])
df = cbind(df, meta[df$Sample,])
df = df[df$Vacc_time != "Evening",]

# Plot summary
df$Stimulus = gsub("M. tuberculosis", "M. tb", as.character(df$Stimulus))
df$Timepoint = as.factor(df$Time)
levels(df$Timepoint) = c("b", "2w", "3m")
fname = file.path(out.dir, "cytokines_summary_expr.pdf")
p = ggplot(data=df, aes(x=Timepoint,  y=Value,  color=Time)) + 
  geom_violin() + 
  geom_jitter(size=.3, alpha=0.2) + 
  facet_grid(~ Cytokine + Stimulus) + xlab("Time point") + ylab("Log cytokine expression") +
  stat_compare_means(label = "p.format", paired = T, hide.ns = T, size=2) + 
  labs(fill = "Time") + 
  theme_light() + 
  theme(text = element_text(size=7), legend.position = "none") + 
  ylim(0, 4.5) + 
  geom_pointrange(size=0.07, 
                  color="black",
                  stat = "summary", position = position_dodge2(width=0.6),
                  fun.min = function(z) {quantile(z, 0.25)},
                  fun.max = function(z) {quantile(z, 0.75)},
                  show.legend = F,
                  fun = median)
ggsave(fname, width = 2.7, height = 2, plot = p)
message(sprintf("Written %s", fname))

# Show folds
C2 = obj$assays$CytokineFold2$data
C3 = obj$assays$CytokineFold3$data
C2 = C2[,colnames(C2) %in% row.names(cm)]
C3 = C3[,colnames(C3) %in% row.names(cm)]
keep = unique(df$Sample)
C2 = C2[keep,]
C3 = C3[keep,]
cf2 = melt(as.matrix(C2))
cf3 = melt(as.matrix(C3))
cf2$Timepoint = "2w"
cf3$Timepoint = "3m"
cf = rbind(cf2, cf3)
colnames(cf) = c("Sample", "Cytokine", "FC", "Timepoint")
cf$Sample = as.character(cf$Sample)
cf$Cytokine = as.character(cf$Cytokine)
cf[,colnames(cm)] = cm[cf$Cytokine,]
cf[1, "Time"] = 1
cf$Stimulus = gsub("M. tuberculosis", "M. tb", as.character(cf$Stimulus))

# Fold changes
agg = aggregate(cf$FC, by=list(Cytokine=cf$Cytokine, Timepoint=cf$Timepoint), median, na.rm=T)
fname = file.path(out.dir, "cytokines_summary_fc.pdf")
p = ggplot(data=cf, aes(x=Timepoint,  y=FC, color=Time)) + 
  geom_hline(yintercept = 1, color="black", size=.3) + 
  geom_jitter(size=.3, alpha=0.2) + 
  facet_grid(~ Cytokine + Stimulus) + xlab("Time point") + 
  ylab("Fold change versus baseline") +
  labs(fill = "Time") + 
  theme_light() + 
  theme(text = element_text(size=7), legend.position = "none") + 
  geom_pointrange(size=0.07, 
                  color="black",
                  stat = "summary", position = position_dodge2(width=0.6),
                  fun.min = function(z) {quantile(z, 0.25)},
                  fun.max = function(z) {quantile(z, 0.75)},
                  show.legend = F,
                  fun = median) + 
  ylim(0.5, 1.5)
ggsave(fname, width = 2.7, height = 2, plot = p)
message(sprintf("Written %s", fname))
fname = file.path(out.dir, "cytokines_summary_fc.csv")
write.csv(cf, fname, row.names = F)
message(sprintf("Written %s", fname))
fname = file.path(out.dir, "cytokines_summary_fc_median.csv") 
write.csv(agg, fname, row.names = F)
message(sprintf("Written %s", fname))














# Create figure for correlations between cytokines at the two time points
for(t in c(2:3)){
  if(t == 2) {C = C2}
  if(t == 3) {C = C3}
  data = data.frame()
  for(c1 in colnames(C)){
    for(c2 in colnames(C)){
      ct = cor.test(C[,c1], C[,c2], method = "spearman")
      fc = 0
      pval = 1
      if(c1 == c2){
        fc = median(C[,c1], na.rm = T)
        tt = t.test(C[,c1]-1)
        pval = tt$p.value
      }
      d = data.frame(Time=t, ResponseA=c1, ResponseB=c2, 
                     Corr=ct$estimate, Corr.pval=ct$p.value, Fold=fc, Fold.pval=pval)
      d[,"CytokineA"] = cm[c1, "Cytokine"]
      d[,"StimulusA"] = cm[c1, "Stimulus"]
      d[,"CytokineB"] = cm[c2, "Cytokine"]
      d[,"StimulusB"] = cm[c2, "Stimulus"]
      data = rbind(data, d)
    }
  }
  
  # Create labelss
  data$Mark = unlist(lapply(pmin(3,floor(-log10(data$Fold.pval))), function(t) paste0(rep("*", t), collapse = "")))
  data$Label = ""
  inxs = data$Fold > 0
  data[inxs, "Label"] = sprintf("%.2f%s", data[inxs, "Fold"], data[inxs, "Mark"])
  
  # Threshold correlations
  data$Spearman = data$Corr
  data[data$Corr.pval > alpha, "Spearman"] = NA
  data$StimulusA = gsub("M. tuberculosis", "M. tb", as.character(data$StimulusA))
  data$StimulusB = gsub("M. tuberculosis", "M. tb", as.character(data$StimulusB))
  data$CytokineB = factor(as.character(data$CytokineB), levels=rev(c("IFNg", "IL1b", "IL6", "TNF")))
  data = data[data$StimulusA == data$StimulusB,]
  
  fname = file.path(out.dir, sprintf("correlations_enrichment_per_stimulus_%d.pdf", t))
  ggplot(data=data) + geom_tile(mapping = aes(x=CytokineA, y=CytokineB, fill=Spearman)) + 
    theme(axis.text.x = element_text(angle = 90, vjust = .5, hjust = 1)) + xlab("") + ylab("") +
    scale_fill_gradient(low = "gray", high = "firebrick", na.value = NA) +
    facet_grid(StimulusB~StimulusA, scales = "free", space = "free") + 
    geom_text(aes(x=CytokineA, y=CytokineB, label=Mark), col="white", size=2) + 
    theme(text = element_text(size=7), 
          legend.position = "top", legend.margin = ggplot2::margin(1,1,1,1)) + 
    theme(legend.key.size = unit(1.,"line")) 
  ggsave(fname, width = 2, height = 2)
  message(sprintf("Written %s", fname))
  fname = file.path(out.dir, sprintf("correlations_enrichment_per_stimulus__%d.csv", t))
  write.csv(data, fname)
}
