#!/usr/bin/env Rscript

hlp = " Combine species into a single heatmap "

# Libs
require(ggplot2)
require(ggpubr)
require(pheatmap)

# Paths
in.data = "./data/data.Robj"
in.assoc2 = "./output/response/CytokineFold2/results_filtered.csv"
in.assoc3 = "./output/response/CytokineFold3/results_filtered.csv"
in.mt.train = "./output/response/trained/results.csv"
in.ms.spec = "./output/response/specific/results.csv"
out.dir = sprintf("./output/response/figure_cytokine_response_combined/")
dir.create(out.dir, recursive = T, showWarnings = F)

# Relevant responses
cy.trained = c("IL1b", "IL6", "TNF")
st.trained = c("S. aureus")
cy.spec = c("IFNg")
st.spec = c("M. tuberculosis")

# Colors
TRAINED = "#0dafdb"
SPECIFIC = "#db7b14"

# Thresholds
save = F
padj.msp = .2

# Load data object
load(in.data)
obj = .GlobalEnv$obj

# Load anova MSPs and filter by response type
mt = read.csv(in.mt.train, stringsAsFactors = F)
mt = mt[mt$Padj < padj.msp,]
mt$Mode = "Trained"
message(sprintf("Selected %d species for mode trained", nrow(mt), mode))
ms = read.csv(in.ms.spec, stringsAsFactors = F)
ms = ms[ms$Padj < padj.msp,]
ms$Mode = "Specific"
message(sprintf("Selected %d species for mode specific", nrow(ms)))
msp = rbind(mt, ms)
msp = msp[!duplicated(msp$Feature),]
row.names(msp) = msp$Feature

# Write augmented data object 
if(save){
  keep = msp$Feature
  S = obj$assays$MSPCore$data[,keep]
  sm = obj$assays$MSPCore$metadata[keep,]
  sm$Mode = msp[row.names(sm), "Mode"]
  sm$Estimate = msp[row.names(sm), "Estimate"]
  aname = sprintf("MSPCore_from_anova")
  obj$assays[[aname]] = list(data=S, metadata=sm)
  save(obj, file=in.data)
  message(sprintf("Written augmented data %s in %s", aname, in.data))
}

# Load individual assocations
as2 = read.csv(in.assoc2, stringsAsFactors = F)
as3 = read.csv(in.assoc3, stringsAsFactors = F)
rf = rbind(as2, as3)
rf = rf[rf$Feature %in% msp$Feature,]
rf$Mode = msp[rf$Feature, "Mode"]
rf$label = msp[rf$Feature, "label"]

# Keep only fitted responses
keep = (rf$Cytokine %in% cy.trained & rf$Stimulus %in% st.trained) | 
       (rf$Cytokine %in% cy.spec & rf$Stimulus %in% st.spec)
rf = rf[keep,]

# Create row an column annotations
ar = rf[!duplicated(rf$Feature),]; row.names(ar) = ar$Feature
ac = rf[!duplicated(rf$Target),]; row.names(ac) = ac$Target
ac = ac[order(ac$Time, ac$Stimulus, ac$Cytokine),]
gaps_col = which(ac$Time == "d3")[1] - 1

# Relabel
arf = ar[,c("species_score", "Mode"), drop=F]
arf$species_score = arf$species_score * 100
colnames(arf) = c("Annotation score %", "Immunomodulation")

acf = ac[,c("Time", "Stimulus")]
acf$Time = factor(acf$Time)
levels(acf$Time) = c("2 weeks", "3 months")

# Recolor
imm        <- c(SPECIFIC, TRAINED)
names(imm) <- c("Specific", "Trained")
ann_colors <- list(Immunomodulation = imm)

# Create matrix
X = matrix(NA, nrow = nrow(arf), ncol=nrow(acf))
row.names(X) = row.names(arf); colnames(X) = row.names(acf)
X[cbind(rf$Feature, rf$Target)] = rf$coef

# Labels
acf$Associations = colSums(!is.na(X))
arf$Associations = rowSums(!is.na(X))
labels_col = sprintf("%s + %s", ac$Cytokine, ac$Stimulus)
labels_row = sprintf("(%d) %s ", arf$Associations, ar$label)

# Compute clustering on imputed data
Y = abs(X) + 0
Y[is.na(Y)] = 0
hcr = hclust(dist(Y))

# Explained variance
V = sign(X) * X**2
M = as.character(sprintf("%.1f", 100*V))
M[is.na(V)] = ""
Mx = matrix(M, ncol = ncol(V))


# Combined heatmap sorted by time
width = 4.2
height = 4.5
fname = file.path(out.dir, "combined_heatmap_mini.pdf")
pheatmap(100 * V, 
         na_col = "white",
         display_numbers = Mx,
         annotation_row = arf, 
         annotation_col = acf,
         gaps_col = gaps_col,
         labels_col = labels_col,
         labels_row = labels_row,
         cluster_rows = hcr,
         cluster_cols = F,
         filename = fname,
         annotation_legend = F,
         annotation_colors = ann_colors,
         treeheight_row = 0,
         width=width, height = height,
         fontsize = 7)
message(sprintf("Written %s", fname))


# Combined heatmap sorted by time
width = 4.2
height = 4.5
fname = file.path(out.dir, "combined_heatmap_legend.pdf")
pheatmap(100 * V,
         na_col = "white",
         annotation_row = arf,
         annotation_col = acf,
         gaps_col = gaps_col,
         labels_col = labels_col,
         labels_row = labels_row,
         cluster_rows = hcr,
         cluster_cols = F,
         annotation_colors = ann_colors,
         filename = fname,
         width=width, height = height,
         fontsize = 7)
message(sprintf("Written %s", fname))
