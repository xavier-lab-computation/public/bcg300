# 300 BCG microbiome analysis documentation

This repository contains the code to reproduce the analysis in the 
study investigating the effects of gut microbiota on Bacillus Calmette-Guerin (BCG)
vaccination of 321 healthy Dutch individuals. The results are presented in the 
paper
    
*The influence of the gut microbiome on BCG-induced trained immunity*

by

Martin Stražar, Vera P. Mourits, Valerie A.C.M. Koeken, 
L. Charlotte J. de Bree, Simone J.C.F.M. Moorlag,
Leo A.B. Joosten, Reinout van Crevel, Hera Vlamakis, Mihai G. Netea, Ramnik J. Xavier

(2021, submitted)

## Data storage

All data used in the study is stored in the R data object `data.Robj`. 
This object is loaded in every script as variable `obj`. 

The object contains the following data objects called assays. Each assay
consist of two entries `data` (numerical data matrix of samples and features) 
and `metadata` (additional information on features).

For example, abundance of 345 Metagenomic species pangenomes (MSPs) is stored as 
in `obj$MSPCore$data`,  while their phylum, genus and species annotation is found in 
`obj$MSPCore$metadata`. 

General:
* `obj$metadata` Meta data on study participants (age, sex, previous vaccinations, ...). 

Metagenomics:
* `obj$assays$MSPCore`	 matrix size 320 x 345. Samples and core MSP abundance computed by MSP miner.
* `obj$assays$MSPOther`	 matrix size 320 x 4938. Samples and non-core MSP abundance computed by MSP miner.
* `obj$assays$MSPGenes`	 matrix size 323 x 86430. Genes encoded by Roseburia, Eubacterium, Bifidobacterium, Ruminococcus, Coprococcus.
* `obj$assays$Species`	 matrix size 313 x 202. Relative abundance of reference species (MetaPhlan 2.2). 

Untargeted Metabolomics:
* `obj$Metabolites` matrix size 321 x 1607. Intensity of 1,607 untargeted metabolite features. 
* `obj$assays$PathwaysKEGG`	 metadata size 350 x 2. Detected KEGG pathways and their names.  

Cytokine production assays (ELISA):
* `obj$assays$Cytokines`	 matrix size 321 x 21. Cytokine expressions at baseline, 2 weeks and 3 months after vaccination.
* `obj$assays$CytokineFold2`	 matrix size 321 x 7. Cytokine expressions fold changes versus baseline 2 weeks after vaccination.
* `obj$assays$CytokineFold3`	 matrix size 321 x 7. Cytokine expressions fold changes versus baseline 3 months after vaccination.



## Analysis scripts

The scripts to generate shall be run from the project source directory.
Details about the used methods can be found in code and the paper. 
   
### Microbiome composition

Summarize the composition

    ./code/composition/plot_msps.R

Compute abundance of individual phyla.

    ./code/composition/plot_abund_phylum.R

Cluster determination on the MSP level. Compute MDS based on Jensen-Shannon distance.
Choose number of clusters where between-within distance begins to rise. 
   
    ./code/composition/clustering.R

Plot dominant phyla on the MDS plot.
    
    ./code/composition/plot_cluster_phyla.R

Compare with 500 FG cohort using the Metaphlan 2 derived species relative abundance.

    ./code/composition/plot_cohorts_500fg.R
    
  
### Microbiome associations

Make another data object to rename variables and be on the safe side.
    
Test microbiome cluster associations with meta data variables.
    
    ./code/associations/plot_cluster_assoc.R
    
Test microbiome cluster associations with cytokines.

    ./code/associations/plot_cluster_assoc_cyto.R
    
Test changes in species with metadata variables.
   
    ./code/associations/plot_maaslin_assoc.R
    

### Finding immunomodulatory species

Plot statistics for individual cytokine responses.

    ./code/response/cytokine_response_stats.R

Run trained immunity and specific response linear models.

    ./code/response/cytokine_response_lm.R trained
    ./code/response/cytokine_response_lm.R specific
    
Compute individual effects of immunomodulatory species on cytokine expression fold changes versus baseline
after 2 weeks and 3 months.

    ./code/response/cytokine_response_lm_indiv.R  CytokineFold2 
    ./code/response/cytokine_response_lm_indiv.R  CytokineFold3 
    
Draw a merged figure of species and individual responses.

    ./code/response/figure_cytokine_response_combined.R


### Metabolomics
  
Quality control of metabolomics data: clustering with respect to HMDB molecular classes.

    ./code/metabolomics/cluster_mbx.R
  
Compute Canonical correlation analysis (CCA) between MSPs and metabolites. 
Plot explained variance per component.

    ./code/metabolomics/cca_mbx_msp.R
        
Compute abundance correlations between arbitrary assays below adjusted p-value < 5%.

    ./code/metabolomics/correlations_pairwise.R

Compare immunomodulatory species to metabolomics. 

    ./code/metabolomics/figure_msps_metabolites.R


Compute correlations between all metabolites and cytokine responses using the log linear model.
Highlight SCFAs.

    ./code/response/cytokine_response_lm_scfas.R trained
    ./code/response/cytokine_response_lm_scfas.R specific

Compare the presence of butyrate producers to trained immunity response.

    ./code/response/cytokine_butyrate.R 


### Phylogenetic trees

Visualize tree of all species and subset including Roseburia.

    ./code/phylophlan/phylo_tree.R
    
### Pathway analysis

Pathway analysis is based on linking data on enzymes and compounds from KEGG pathway graphs.
The same analysis can be repeated for any pathway in KEGG, provided that the a file
that links pathway, compound and enzyme IDS. Example files are stored in `./output/pathways/kgml/`.

    map00360: Phenylalanine metabolism.
    map00380: Tryptophan metabolism.
    
Example file for Phenylalanine metabolism (`map00360.graph.csv`). 
Each line stores a forward and (potential) reverse reaction. 

    Reaction,SubstrateID,ProductID,EnzymeID,Substrate,Product,Enzyme
    rn:R02611,C05853,C00601,K00055,Phenylethyl alcohol,Phenylacetaldehyde,
    rev:rn:R02611,C00601,C05853,K00055,Phenylacetaldehyde,Phenylethyl alcohol,
    rn:R06941,C14145,C02232,K00074,(3S)-3-Hydroxyadipyl-CoA,3-Oxoadipyl-CoA,paaH
    rn:R02536,C00601,C07086,K00129,Phenylacetaldehyde,Phenylacetic acid,ALDH3
    
Based on panel of genera, select pathways to investigate in more detail.

    ./code/pathways/msps_compounds.R Roseburia map00360
    ./code/pathways/msps_compounds.R Ruminococcus map00360
    ./code/pathways/msps_compounds.R Coprococcus map00360
    ./code/pathways/msps_compounds.R Eubacterium map00360
    ./code/pathways/msps_compounds.R Bifidobacterium map00360
    
Aggregate pathway compound correlation for all genera.

    ./code/pathways/msps_compounds_merge.R map00360

Presence and abundance of pathway enzymes in selected panel of genera.

    ./code/pathways/msps_enzymes.R map00360
    
   
### Phenylalanine metabolism details.

Detailed look at enzymes and compounds within phenylalanine metabolism for Roseburia species.   

Compute correlations between groups of enzymes and compounds (called branches) in phenylalanine metabolism.

    ./code/pathways/enzymes_compounds.R 

Compare pathway branch activities for randomly sampled groups of genes.    
    
    ./code/pathways/enzymes_compounds_modes.R
   
## Dependencies

The scripts depend on one or more the following R packages listed in 

    ./requirements.R

The versions were as follows:
    
    R version 3.6.1 (2019-07-05)
    Platform: x86_64-apple-darwin15.6.0 (64-bit)
    Running under: macOS Mojave 10.14.6
    
    Matrix products: default
    BLAS:   /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRblas.0.dylib
    LAPACK: /Library/Frameworks/R.framework/Versions/3.6/Resources/lib/libRlapack.dylib
    
    locale:
    [1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8
    
    attached base packages:
    [1] stats     graphics  grDevices utils     datasets  methods   base     
    
    other attached packages:
     [1] stringr_1.4.0     scales_1.0.0      reshape2_1.4.3    pvclust_2.2-0    
     [5] plyr_1.8.4        philentropy_0.4.0 pheatmap_1.0.12   mltools_0.3.5    
     [9] mclust_5.4.5      ggtree_2.0.4      ggrepel_0.8.2     ggpubr_0.2.4     
    [13] magrittr_1.5      ggplot2_3.3.0     entropy_1.2.1     data.table_1.12.8
    [17] ape_5.4-1         Rtsne_0.15        Maaslin2_1.0.0   
    
    loaded via a namespace (and not attached):
     [1] treeio_1.10.0       biglm_0.9-1         tidyselect_0.2.5   
     [4] purrr_0.3.3         lattice_0.20-38     pcaPP_1.9-73       
     [7] colorspace_1.4-1    vctrs_0.3.4         getopt_1.20.3      
    [10] rlang_0.4.7         pillar_1.4.2        optparse_1.6.4     
    [13] glue_1.3.1          withr_2.1.2         RColorBrewer_1.1-2 
    [16] rvcheck_0.1.8       lifecycle_0.1.0     robustbase_0.93-5  
    [19] munsell_0.5.0       ggsignif_0.6.0      gtable_0.3.0       
    [22] mvtnorm_1.0-11      parallel_3.6.1      DEoptimR_1.0-8     
    [25] Rcpp_1.0.3          BiocManager_1.30.10 jsonlite_1.6       
    [28] lpsymphony_1.14.0   stringi_1.4.3       dplyr_0.8.3        
    [31] grid_3.6.1          tools_3.6.1         lazyeval_0.2.2     
    [34] tibble_2.1.3        crayon_1.3.4        tidyr_1.0.0        
    [37] pkgconfig_2.0.3     Matrix_1.2-17       tidytree_0.3.3     
    [40] assertthat_0.2.1    R6_2.4.1            nlme_3.1-140       
    [43] compiler_3.6.1  


## License

Copyright 2021 Broad Institute of MIT and Harvard, Cambridge, MA

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
